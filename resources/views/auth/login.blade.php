@extends('layouts.frontend')
@section('content')
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui red header">
                <span class="content">
                    Log-in to your account
                </span>
            </h2>
            <form action="{{ route('login') }}" method="post" class="ui large form">
                @csrf

                <div class="ui stacked segment">
                    <div class="field{{ $errors->has('email') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="email" id="email" placeholder="E-mail address">
                        </div>
                    </div>
                    <div class="field{{ $errors->has('password') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" id="password" placeholder="Password">
                        </div>
                    </div>
                    <button type="submit" class="ui fluid large red submit button">Login</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('style')
    <style type="text/css">
        body {
            background-color: #DADADA;
        }
        body > .grid {
            height: 100%;
        }
        .column {
            max-width: 450px;
        }
    </style>
@endsection