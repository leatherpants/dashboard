<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard</title>
    <meta name="description" content="Personal dashboard">
    <link rel="stylesheet" href="{{ asset('assets/semantic.min.css') }}">
    @auth
    <style type="text/css">
        body {
            background-color: #FFFFFF;
        }
        .main.container {
            margin-top: 7em;
        }
        .ui.list>.item .description {
            text-overflow:ellipsis;
            overflow: hidden;
            white-space:nowrap;
        }
    </style>
    @endauth
@yield('style')
</head>
<body>
@auth
    <div class="ui fixed inverted menu">
        <div class="ui fluid container">
            <a href="{{ route('dashboard') }}" class="header item">
                Dashboard
            </a>
            <div class="right menu">
                <a href="{{ route('logout') }}" class="icon item">
                    <i class="sign out icon"></i>
                </a>
            </div>
        </div>
    </div>
@endauth
@yield('content')
<script src="{{ asset('assets/jquery.min.js') }}"></script>
<script src="{{ asset('assets/semantic.min.js') }}"></script>
</body>
</html>