@extends('layouts.frontend')
@section('content')
    <div class="ui main container">
        <div class="ui stackable grid">
            <div class="four wide column">
                <div class="ui red segment">
                    <div class="ui image">
                        <img src="https://media.ferrari.granath/users/sandra-granath.jpg" alt="{{ auth()->user()->display_name }}">
                        <div class="ui red top left attached label">{{ auth()->user()->display_name }}</div>
                    </div>
                </div>
            </div>
            <div class="eight wide column">
                <div class="ui red segment">
                    <h2 class="ui header">
                        <i class="github icon"></i>
                        <span class="content">
                            Github
                        </span>
                    </h2>
                    <div class="ui relaxed divided list">
                        <div class="item">
                            <div class="content">
                                <a href="https://github.com/laravel/framework" target="_blank" rel="noopener" class="header">laravel/framework</a>
                                <div class="description"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <a href="https://github.com/laravel/laravel" target="_blank" rel="noopener" class="header">laravel/laravel</a>
                                <div class="description">A PHP framework for web artisans</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <a href="https://github.com/nunomaduro/larastan" target="_blank" rel="noopener" class="header">nunomaduro/larastan</a>
                                <div class="description">Larastan - Discover bugs in your code without running it. A Phpstan wrapper for Laravel.</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <a href="https://github.com/Semantic-Org/Semantic-UI" target="_blank" rel="noopener" class="header">Semantic-Org/Semantic-UI</a>
                                <div class="description">Semantic is a UI component framework based around useful principles from natural language.</div>
                            </div>
                        </div>
                    </div>
                    <p>
                        <a href="">Github</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection