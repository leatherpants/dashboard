<?php

namespace App\Http\Controllers;

use App\dashboard\Users\Requests\LoginRequest;

class LoginController extends Controller
{
    /**
     * Handle a login request.
     * @param LoginRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(LoginRequest $request)
    {
        $details = $request->only('email', 'password');

        if (auth()->attempt($details)) {
            return redirect()
                ->route('dashboard');
        }

        return redirect()
            ->route('index');
    }

    /**
     * Handle a logout request.
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        auth()->logout();

        return redirect()
            ->route('index');
    }
}
