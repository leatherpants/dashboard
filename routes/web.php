<?php

use Illuminate\Support\Facades\Route;

Route::get('', ['uses' => 'HomeController@index', 'as' => 'index']);

Route::post('login', ['uses' => 'LoginController@login', 'as' => 'login']);
Route::get('logout', ['uses' => 'LoginController@logout', 'as' => 'logout']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('dashboard', ['uses' => 'HomeController@dashboard', 'as' => 'dashboard']);
});
