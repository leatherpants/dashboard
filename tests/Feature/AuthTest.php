<?php

namespace Tests\Feature;

use Tests\TestCase;

class AuthTest extends TestCase
{
    /** @test */
    public function test_successful_login()
    {
        $response = $this->post(route('login'), ['email' => $this->user->email, 'password' => 'password']);
        $response->assertStatus(302);
        $response->assertRedirect(route('dashboard'));
    }

    /** @test */
    public function test_successful_login_fails()
    {
        $response = $this->post(route('login'), ['email' => $this->user->email, 'password' => 'secret']);
        $response->assertStatus(302);
        $response->assertRedirect(route('index'));
    }

    /** @test */
    public function test_successful_logout()
    {
        $response = $this->actingAs($this->user)->get(route('logout'));
        $response->assertStatus(302);
        $response->assertRedirect(route('index'));
    }

    /** @test */
    public function test_tries_to_access_the_dashboard()
    {
        $response = $this->get(route('dashboard'));
        $response->assertStatus(302);
        $response->assertRedirect(route('index'));
    }
}
